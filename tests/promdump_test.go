package rdlt_test

import (
	"bytes"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
	"net/http"
)

type localResponseWriter struct {
	headers http.Header
	buf     io.Writer
}

func (l *localResponseWriter) Header() http.Header {
	return l.headers
}

func (l *localResponseWriter) Write(bytes []byte) (int, error) {
	return l.buf.Write(bytes)
}

func (l *localResponseWriter) WriteHeader(statusCode int) {

}

func Dump() string {
	rq, _ := http.NewRequest(http.MethodGet, "http://localhost/metrics", nil)
	bts := bytes.NewBuffer(nil)
	rw := &localResponseWriter{
		headers: map[string][]string{},
		buf:     bts,
	}
	promhttp.Handler().ServeHTTP(rw, rq)
	return bts.String()
}
