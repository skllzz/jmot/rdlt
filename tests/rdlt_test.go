package rdlt_test

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/skllzz/jmot/rdlt"
	"gitlab.com/skllzz/jmot/rdlt/logging"
	"gitlab.com/skllzz/multiop"
	"go.uber.org/zap"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func init() {
	testing.Init()
	flag.Parse()
	logging.InitLogger(&logging.LoggerConfig{
		TraceMode: false,
	})
	<-rdlt.InitRedis()
}

type SimpleTask struct {
	Session string
	Name    string
}

func TestNestedLocks(t *testing.T) {
	ctx := rdlt.BreakableBackgroundContext
	lname := uuid.NewString()
	wg := new(sync.WaitGroup)
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func() {
			rdlt.WithNamedLock(ctx, lname, func(ctx context.Context) error {
				defer wg.Done()
				time.Sleep(time.Second)
				println("Acting")
				return nil
			})
		}()
	}
	wg.Wait()
}
func TestLockTimout(t *testing.T) {
	ctx := rdlt.BreakableBackgroundContext
	lname := uuid.NewString()
	if err := rdlt.WithNamedLock(ctx, lname, func(ctx context.Context) error {
		t.Log("Grab internal lock with second timeout\n")
		return rdlt.WithNamedLock(rdlt.WithInitialGrabTimeout(ctx, 0), lname, func(ctx context.Context) error {
			return nil
		})
	}); err != nil {
		t.Logf("ERROR %v\n", err)
		if errors.Is(err, rdlt.LockTimeoutError) {
			return
		}
	}
	t.Fatal("Lock timout does not work")
}

func TestSequentialLock(t *testing.T) {
	ctx := rdlt.BreakableBackgroundContext
	//rdlt.EmulateFail(1*time.Second, 3*time.Second)
	lname := uuid.NewString()
	runner := multiop.NewParallelRunner(ctx, 10)
	defer runner.Dispose()
	for i := 0; i < 100; i++ {
		if err := runner.Submit(func(ctx context.Context) error {
			return rdlt.WithNamedLock(ctx, lname, func(ctx context.Context) error {
				t.Log("Grab internal lock")
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				return nil
			})
		}); err != nil {
			t.Fatalf("ERROR %v\n", err)
		}
	}
	if err := runner.Await(); err != nil {
		t.Fatal(err)
	}
}
func TestLockWait(t *testing.T) {
	ctx, cancel := context.WithCancel(rdlt.BreakableBackgroundContext)
	defer cancel()
	runner := multiop.NewParallelRunner(ctx, 10)
	defer runner.Dispose()
	mv := make(map[int]int)
	mm := new(sync.Mutex)
	//rdlt.EmulateFail(1*time.Second, 3*time.Second)
	for i := 0; i < 1000; i++ {
		if err := runner.Submit(func(ctx context.Context) error {
			shard := int(rand.Int31n(10))
			shard2 := int(rand.Int31n(10)) + 20
			e := rdlt.WithContextLocks(rdlt.NamedLocks(rdlt.WithInitialGrabTimeout(ctx, 1*time.Second), fmt.Sprintf("lock-%v", shard), fmt.Sprintf("slock-%v", shard2)), func(ctx context.Context) error {
				mm.Lock()
				v := mv[shard]
				v2 := mv[shard2]
				mm.Unlock()
				v++
				v2++
				mm.Lock()
				mv[shard] = v
				mv[shard2] = v2
				mm.Unlock()
				t.Logf("lock started %v %v", shard, shard2)
				select {
				case <-ctx.Done():
				case <-time.After(time.Millisecond * time.Duration(rand.Int31n(10)+1)):
				}
				t.Logf("lock completed")
				v--
				v2--
				mm.Lock()
				mv[shard] = v
				mv[shard2] = v2
				mm.Unlock()
				return ctx.Err()
			})
			if errors.Is(e, rdlt.LockTimeoutError) {
				t.Logf("Deadlock timeout")
				return nil
			}
			return e
		}); err != nil {

			t.Fatal(err)

		}
	}
	if err := runner.Await(); err != nil {
		t.Fatal(err)
	}
	t.Logf("%v", mv)
	if len(mv) == 0 {
		t.Fatal("no tasks was run")
	}
	for _, v := range mv {
		if v != 0 {
			t.Fatal("element is not empty")
		}
	}
}

func TestTaskWait(t *testing.T) {
	ctx := rdlt.BreakableBackgroundContext
	ok := false
	canceler := rdlt.HandleTasks(ctx, func(ctx context.Context, task *SimpleTask) error {
		ok = true
		return nil
	})
	defer canceler()
	task := rdlt.CreateTask("", uuid.NewString(), time.Now().Add(time.Second*2), &SimpleTask{})
	if err := rdlt.ScheduleTask(ctx, task); err != nil {
		t.Fatal(err)
	}
	if ok {
		t.Fatal("Task early run")
	}
	if err := task.WaitComplete(ctx); err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatal("Task not run")
	}
}
func TestIfNotExistsTasks(t *testing.T) {
	t.Log("Start")
	session := uuid.NewString()
	gctx := rdlt.WithGroup(rdlt.BreakableBackgroundContext)

	handler := rdlt.HandleTasks(rdlt.BreakableBackgroundContext, func(ctx context.Context, task *SimpleTask) error {
		if task.Session == session {
			logging.Default.Info("Run")
			time.Sleep(time.Second)
		}
		return nil
	})
	defer handler()

	for i := 1; i < 100; i++ {
		logging.Default.Info("Schedule")
		if err := rdlt.ScheduleTask(gctx, rdlt.CreateTask(fmt.Sprintf("%v", rand.Int31n(5)), uuid.NewString(), time.Now(), &SimpleTask{Session: session})); err != nil {
			t.Fatal(err)
		}
	}
	if err := rdlt.WaitGroup(gctx, time.Minute); err != nil {
		t.Fatal(err)
	}
	t.Log("Done")
	<-gctx.Done()
}
func TestGroups(t *testing.T) {
	logging.Default.Info("Started")
	ctx, cancel := context.WithCancel(rdlt.BreakableBackgroundContext)
	defer cancel()
	session := uuid.NewString()
	if session != "" {
	}
	rdlt.HandleTasks(ctx, func(ctx context.Context, task *SimpleTask) error {
		logging.Default.Info("Run")
		if task.Session == session || task.Session == session+"!" {
			logging.Default.Info("Schedule")
			return rdlt.ScheduleTask(ctx, rdlt.CreateTask("", uuid.NewString(), time.Now().Add(time.Second*2), &SimpleTask{Session: task.Session + "!"}))
		}
		time.Sleep(time.Second)
		return nil
	})
	gctx := rdlt.WithGroup(ctx)

	if id := rdlt.GetGroupId(gctx); id != "" {
		logging.Default.Info("Tasks group", zap.String("id", id))
	}
	logging.Default.Info("Schedule")
	if err := rdlt.ScheduleTask(gctx, rdlt.CreateTask("", uuid.NewString(), time.Now().Add(time.Hour*2), &SimpleTask{Session: session})); err != nil {
		return
	}
	if err := rdlt.WaitGroup(gctx, time.Second*3); err != nil {
		t.Fatal(err)
	}
}
func TestTasks(t *testing.T) {
	session := uuid.NewString()
	rdlt.EmulateFail(1*time.Second, 3*time.Second)
	logging.Default.Info("Session", zap.String("session", session))
	ctx, cancel := context.WithCancel(rdlt.BreakableBackgroundContext)
	defer cancel()
	tmap := make(map[string]bool)
	mtx := new(sync.Mutex)
	totalCalls := 0
	uniqueCalls := 0
	if true {
		rdlt.HandleTasks(ctx, func(ctx context.Context, task *SimpleTask) error {
			if task.Session == session {
				meta := rdlt.GetTaskMeta(ctx)
				delay := time.Now().Sub(meta.When)
				t.Logf("Acting task %v %v", task.Name, delay)
				time.Sleep(time.Duration(rand.Int31n(500)) * time.Millisecond)
				if rand.Int31n(20) == 2 {
					//return errors.New("will retry")
				}
				mtx.Lock()
				totalCalls++
				if !tmap[meta.Id] {
					t.Errorf("Repeated call!!! %v %v", task.Name, delay)
				} else {
					uniqueCalls++
				}
				delete(tmap, meta.Id)
				mtx.Unlock()
			} else {
				t.Logf("Ignore other session task %v %v", task.Name, task.Session)
			}
			return nil
		}, rdlt.WithoutQueues)
	}
	//time.Sleep(10 * time.Second)
	if true {
		for c := 0; c < 500 && ctx.Err() == nil; c++ {
			id := fmt.Sprintf("id-%v-%v", session, c)
			mtx.Lock()
			tmap[id] = true
			mtx.Unlock()
			if err := rdlt.ScheduleTask(ctx, rdlt.CreateTask(
				fmt.Sprintf("queue-%v", c%3),
				id,
				time.Now().Add(time.Duration(c*10)*time.Millisecond),
				SimpleTask{
					Name:    id,
					Session: session,
				})); err != nil {
				mtx.Lock()
				delete(tmap, id)
				mtx.Unlock()
				t.Errorf("Unable to add task %v", err)
			}
			time.Sleep(1 * time.Millisecond)
			//if err := rdlt.CancelTask(ctx, id); err != nil {
			//	t.Errorf("Unable to cancel task %v", err)
			//} else {
			//	mtx.Lock()
			//	delete(tmap, id)
			//	mtx.Unlock()
			//}
		}
	}
	//rdlt.EnsureActive()
	t.Log("All planned")
	go func() {
		for ctx.Err() == nil {
			time.Sleep(1 * time.Second)
			mtx.Lock()
			left := len(tmap)
			mtx.Unlock()
			t.Logf("Tasks left %v calls_total:%v calls_unique:%v", left, totalCalls, uniqueCalls)
			if left == 0 {
				break
			}
		}
		t.Log("All done")
		t.Logf("Calls total:%v Calls unique:%v", totalCalls, uniqueCalls)
		time.Sleep(time.Second)
		//cancel()
	}()
	<-ctx.Done()
}
