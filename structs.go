package rdlt

import (
	"time"
)

type Task struct {
	Payload     interface{}
	Id          string
	Type        string
	Queue       string
	When        time.Time
	IfNotExists bool
	key         string
}
