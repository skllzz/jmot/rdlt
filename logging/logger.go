package logging

import (
	"fmt"
	"gitlab.com/skllzz/multiop"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"runtime"
	"strings"
	"sync"
)

var Sugared *WrappedLogger
var Default *zap.Logger

var NoStackTrace = zap.AddStacktrace(&neverLevel{})

type WrappedLogger struct {
	*zap.SugaredLogger
}

func (g *WrappedLogger) Println(args ...interface{}) {
	g.Info(args...)
}

func (g *WrappedLogger) Printf(format string, args ...interface{}) {
	g.Infof(format, args...)
}

func (g *WrappedLogger) Infoln(args ...interface{}) {
	g.Info(args...)
}

func (g *WrappedLogger) Warning(args ...interface{}) {
	g.Warn(args...)
}

func (g *WrappedLogger) Warningln(args ...interface{}) {
	g.Warn(args...)
}

func (g *WrappedLogger) Warningf(format string, args ...interface{}) {
	g.Warnf(format, args...)
}

func (g *WrappedLogger) Errorln(args ...interface{}) {
	g.Error(args...)
}

func (g *WrappedLogger) Fatalln(args ...interface{}) {
	g.Fatal(args...)
}

func (g *WrappedLogger) V(l int) bool {
	return true
}

var lOnce = new(sync.Once)

type neverLevel struct {
}

var Never = &neverLevel{}

func (n neverLevel) Enabled(level zapcore.Level) bool {
	return false
}

type LoggerConfig struct {
	JsonMode   bool
	TraceMode  bool
	SetOptions func(config *zap.Config)
}

func InitLogger(cfg *LoggerConfig) {
	lOnce.Do(func() {
		jsonMode := false
		traceMode := false
		if cfg != nil {
			jsonMode = cfg.JsonMode
			traceMode = cfg.TraceMode
		}
		config := zap.NewDevelopmentConfig()
		if jsonMode {
			config = zap.NewProductionConfig()
		}
		if traceMode {
			config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
		} else {
			config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
		}
		_, fpath, _, _ := runtime.Caller(0)
		fpath = strings.TrimSuffix(fpath, "logging/logger.go")
		config.EncoderConfig.EncodeCaller = func(caller zapcore.EntryCaller, encoder zapcore.PrimitiveArrayEncoder) {
			if strings.HasPrefix(caller.File, fpath) {
				encoder.AppendString(fmt.Sprintf("%v:%v", caller.File[len(fpath):], caller.Line))
				return
			}
			encoder.AppendString(caller.TrimmedPath())
		}
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		if jsonMode {
			config.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
		}
		if cfg != nil && cfg.SetOptions != nil {
			cfg.SetOptions(&config)
		}
		Default, _ = config.Build(zap.AddCaller(), zap.Development(), zap.AddStacktrace(zapcore.ErrorLevel))
		multiop.SetLogger(Default)
		Sugared = &WrappedLogger{Default.Sugar()}
		// Should only be done from init functions
	})
}
