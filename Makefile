.PHONY: init
init:
	#sudo apt install -y protobuf-compiler
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

.PHONY: proto
proto: # clone or symlink git@gitlab.com:skllzz/jmot/api/keeper.git to proto/keeper folder
	rm -rf internal/pb
	mkdir -p internal/pb
	protoc -I proto \
                    --go_out=internal/pb --go_opt=paths=source_relative \
                    $(shell find -L proto -type f -name '*.proto' -printf '%P\n')
	
