package rdlt

import (
	"flag"
)

var redisEndpoint = flag.String("redis-endpoint", "localhost:6379", "redis api endpoint")

func GetRedisEndpoint() string {
	return *redisEndpoint
}
