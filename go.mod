module gitlab.com/skllzz/jmot/rdlt

go 1.21.0

require (
	github.com/google/uuid v1.3.1
	github.com/prometheus/client_golang v1.17.0
	github.com/redis/go-redis/v9 v9.2.1
	gitlab.com/skllzz/multiop v1.0.3
	go.uber.org/atomic v1.11.0
	go.uber.org/zap v1.26.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/matttproud/golang_protobuf_extensions/v2 v2.0.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.45.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.4.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
