package rdlt

import (
	"context"
	"fmt"
	goredislib "github.com/redis/go-redis/v9"
	"gitlab.com/skllzz/jmot/rdlt/logging"
	"go.uber.org/zap"
	"strings"
	"sync"
	"time"
)

var redisReady, redisReadyDone = context.WithCancel(context.Background())

var bmutex = new(sync.Mutex)
var bcast = sync.NewCond(bmutex)
var notifiedKey = ""

func getNotifyChannel(ctx context.Context, key string) <-chan struct{} {
	res := make(chan struct{})
	go func() {
		defer close(res)
		bmutex.Lock()
		defer bmutex.Unlock()
		for ctx.Err() == nil {
			bcast.Wait()
			if key == notifiedKey {
				select {
				case <-ctx.Done():
				case res <- struct{}{}:
				default:
				}
			}
		}
	}()
	return res
}

var initDoneCtx, initDone = context.WithCancel(BreakableBackgroundContext)

func InitRedis() <-chan struct{} {
	initRedisOnce.Do(func() {

		opts = &goredislib.Options{
			Addr:                  *redisEndpoint,
			DialTimeout:           time.Second * 5,
			MaxRetries:            3,
			MinIdleConns:          5,
			ReadTimeout:           time.Second * 5,
			ClientName:            "RDLT-1.0",
			ContextTimeoutEnabled: true,
		}
		client = goredislib.NewClient(opts)
		go func() {
			defer initDone()
			<-BreakableBackgroundContext.Done()
			_ = client.Close()
		}()
		go func() {
			lastTimeKeySent := time.UnixMilli(0)
			t := time.NewTicker(time.Second)
			defer t.Stop()
			for BreakableBackgroundContext.Err() == nil {
				flags := "Egx$s"
				//flags = "AKE"
				if cset, err := client.ConfigGet(BreakableBackgroundContext, "notify-keyspace-events").Result(); err == nil {
					cflags := cset["notify-keyspace-events"]
					for _, c := range flags {
						if !strings.Contains(cflags, string(c)) {
							cflags += string(c)
						}
					}
					flags = cflags
					if err := client.ConfigSet(BreakableBackgroundContext, "notify-keyspace-events", flags).Err(); err == nil {
						channels := make([]string, 0)
						channels = append(channels, fmt.Sprintf("__keyevent@%v__:del", client.Options().DB))
						channels = append(channels, fmt.Sprintf("__keyevent@%v__:expired", client.Options().DB))
						channels = append(channels, fmt.Sprintf("__keyevent@%v__:expire", client.Options().DB))
						channels = append(channels, fmt.Sprintf("__keyevent@%v__:sadd", client.Options().DB))
						channels = append(channels, fmt.Sprintf("__keyevent@%v__:srem", client.Options().DB))
						sub := client.Subscribe(BreakableBackgroundContext, channels...)
						mc := sub.Channel()
						ok := true
						initDone()
						for BreakableBackgroundContext.Err() == nil && ok {
							key := ""
							select {
							case msg := <-mc:
								if msg == nil {
									ok = false
									continue
								}
								key = msg.Payload
								logging.Default.Debug("Receive key event", zap.String("channel", msg.Channel), zap.String("payload", msg.Payload))
							case <-t.C:
							}
							now := time.Now()
							if key == "" && now.Sub(lastTimeKeySent) < time.Second {
								continue
							}
							lastTimeKeySent = now
							bmutex.Lock()
							notifiedKey = key
							bcast.Broadcast()
							bmutex.Unlock()
						}
						sub.Close()
					}
				}
				t.Reset(1 * time.Second)
				select {
				case <-BreakableBackgroundContext.Done():
					return
				case <-t.C:
				}
			}

			_ = client.Close()
		}()

		EnsureActive()
		redisReadyDone()
	})
	return initDoneCtx.Done()
}

func EnsureActive() {
	go runRedisTasks(BreakableBackgroundContext)
}

// Client returns redis client connection
func Client() *goredislib.Client {
	InitRedis()
	return client
}

func WaitRedisClientReady(ctx context.Context) {
	InitRedis()
	logging.Default.Info("Waiting REDIS become ready")
	t := time.NewTicker(time.Second)
	defer t.Stop()
	for ctx.Err() == nil {
		if _, err := client.Ping(ctx).Result(); err == nil {
			break
		} else {
			logging.Default.Warn("Redis not connected", zap.Error(err))
			select {
			case <-ctx.Done():
				return
			case <-t.C:
			}
		}
	}
	logging.Default.Info("REDIS is ready")
}
