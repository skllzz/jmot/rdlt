package rdlt

import (
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"google.golang.org/protobuf/proto"
	"reflect"
)

var protoMarshal = &proto.MarshalOptions{Deterministic: true}

func PayloadName(v interface{}) string {
	if p, ok := v.(proto.Message); ok {
		return string(p.ProtoReflect().Descriptor().FullName())
	}
	t := reflect.Indirect(reflect.ValueOf(v)).Type()
	tn := fmt.Sprintf("%v/%v", t.PkgPath(), t.Name())
	return tn
}

func Marshal(v interface{}) ([]byte, error) {
	if p, ok := v.(proto.Message); ok {
		return protoMarshal.Marshal(p)
	}
	return json.Marshal(v)
}

func Unmarshal(data []byte, v interface{}) error {
	if p, ok := v.(proto.Message); ok {
		return proto.Unmarshal(data, p)
	}
	return json.Unmarshal(data, v)
}

func BytesHash512(b []byte) string {
	csum := sha512.Sum512(b)
	return base64.RawURLEncoding.EncodeToString(csum[:])
}

func StrHash512(s string) string {
	return BytesHash512([]byte(s))
}
