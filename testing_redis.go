package rdlt

import (
	goredislib "github.com/redis/go-redis/v9"
	"math/rand"
	"time"
)

// EmulateFail breaks and restore connection within random delay from min to max
func EmulateFail(min, max time.Duration) {
	InitRedis()
	go func() {
		randomFail := min + time.Duration(rand.Int31n(int32(max.Milliseconds()-min.Milliseconds())))*time.Millisecond
		t := time.NewTimer(randomFail)
		defer t.Stop()
		for BreakableBackgroundContext.Err() == nil {
			select {
			case <-BreakableBackgroundContext.Done():
				continue
			case <-t.C:
				_ = client.Close()
				client = goredislib.NewClient(opts)
				return
			}
		}
	}()
}
