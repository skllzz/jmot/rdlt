package rdlt

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/skllzz/jmot/rdlt/logging"
	"go.uber.org/zap"
	"sync"
	"time"
)

type Mutex struct {
	ctx    context.Context
	cancel context.CancelFunc
	mtx    sync.Mutex
	locked bool
}

var deleteScript = `
	if redis.call("GET", KEYS[1]) == ARGV[1] then
		return redis.call("DEL", KEYS[1])
	else
		return 0
	end
`

var touchScript = `
	if redis.call("GET", KEYS[1]) == ARGV[1] then
		return redis.call("PEXPIRE", KEYS[1], ARGV[2])
	else
		return 0
	end
`

var deleteScriptHash = ""
var touchScriptHash = ""

func releaseLockInContext(ctx context.Context, name string, lockUuid string) error {
	InitRedis()
	if deleteScriptHash != "" {
		if ok, err := client.ScriptExists(ctx, deleteScriptHash).Result(); err != nil {
			return err
		} else {
			if !ok[0] {
				deleteScriptHash = ""
			}
		}
	}
	if deleteScriptHash == "" {
		if hash, err := client.ScriptLoad(ctx, deleteScript).Result(); err != nil {
			return err
		} else {
			deleteScriptHash = hash
		}
	}
	return client.EvalSha(ctx, deleteScriptHash, []string{name}, lockUuid).Err()
}
func touchLockInContext(ctx context.Context, name string, lockUuid string, duration time.Duration) error {
	InitRedis()
	if touchScriptHash != "" {
		if ok, err := client.ScriptExists(ctx, touchScriptHash).Result(); err != nil {
			return err
		} else {
			if !ok[0] {
				touchScriptHash = ""
			}
		}
	}
	if touchScriptHash == "" {
		if hash, err := client.ScriptLoad(ctx, touchScript).Result(); err != nil {
			return err
		} else {
			touchScriptHash = hash
		}
	}
	if status, err := client.EvalSha(ctx, touchScriptHash, []string{name}, lockUuid, duration.Milliseconds()).Result(); err != nil {
		return err
	} else {
		if status == int64(0) {
			return errors.New("mutex lost")
		}
	}
	return nil
}

func grabLockInContext(ctx context.Context, name string, lockUuid string, onFail func()) error {
	InitRedis()
	if lockUuid == "" {
		lockUuid = uuid.NewString()
	}
	var wc <-chan struct{}
	if initialGrabTimout, ok := ctx.Value(initialGrabMutexTimout).(time.Duration); !ok || initialGrabTimout > 0 {
		wc = getNotifyChannel(ctx, name)
	}
	t := time.NewTicker(time.Second * 5)
	defer t.Stop()
	for ctx.Err() == nil {
		if ok, err := client.SetNX(ctx, name, lockUuid, time.Second*8).Result(); err != nil {
			return err
		} else {
			if ok {
				break
			}
		}
		if wc == nil {
			return LockTimeoutError
		}
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-wc:
		case <-t.C:
		}
	}
	go func() {
		et := time.NewTicker(3 * time.Second)
		defer et.Stop()
		for ctx.Err() == nil {
			select {
			case <-ctx.Done():
				return
			case <-et.C:
			}
			if ctx.Err() == nil {
				if err := touchLockInContext(ctx, name, lockUuid, time.Second*8); err != nil {
					if ctx.Err() == nil {
						logging.Default.Error("unable to extend mutex", zap.String("mutex", name), zap.Error(err))
						if onFail != nil {
							onFail()
						}
						return
					}
				}
			}
		}
	}()
	return ctx.Err()
}
