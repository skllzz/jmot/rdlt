package rdlt

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	goredislib "github.com/redis/go-redis/v9"
	"gitlab.com/skllzz/jmot/rdlt/internal/pb"
	"gitlab.com/skllzz/jmot/rdlt/logging"
	"gitlab.com/skllzz/multiop"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/protobuf/types/known/timestamppb"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

// MaxTaskDelayBeforeDrop Максимальное время повтора задачи которая завершается ошибкой.
// После этого она еще столько же полежит в ключах и очереди без попыток
// выполнения и только потом будет полностью удалена
var MaxTaskDelayBeforeDrop = time.Hour * 24 * 15

const tasksChangesStream = "rdlt-tasks-changes"
const allTasksSet = "rdlt-all-tasks-set"
const taskPayloadPrefix = "rdlt-tasks-payload-prefix"
const taskSupplierLockId = "cee9b757-00ea-4ed3-bfdb-7a530a53d2af"
const payloadKeyName = "payload_key_id"
const groupMaxTTL = time.Minute * 30

// bucketSize размер минимальной порции данных при работе с задачами
const bucketSize = 32
const DefaultConcurrency = 16

func breakableBackgroundContext() context.Context {
	bctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGTERM)
	return bctx
}

var BreakableBackgroundContext = breakableBackgroundContext()

var client *goredislib.Client
var initRedisOnce = new(sync.Once)
var runRedisTasksOnce = new(sync.Once)

var taskMeta = "taskMeta"
var taskGroupId = "taskGroupId"

var droppedTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "dropped"),
	Help: "Количество отброшенных по времени задач которые, так и не смогли успешно отработать за максимальный период " + MaxTaskDelayBeforeDrop.String(),
}, []string{"type"})

var retryTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "retry"),
	Help: "Количество задач помещенных в очередь повторно из за ошибки выполнения",
}, []string{"type"})

var queueLockRetryTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "queue_lock_retry"),
	Help: "Количество задач отправленных на повтор из за невозможности забрать блокировку имя очереди",
}, []string{"type"})

var scheduledTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "scheduled"),
	Help: "Всего запросов на планирование задач",
}, []string{"type"})

var queuedTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "queued"),
	Help: "Количество задач перемещенных в очередь на исполнение",
}, []string{"type"})

var recoveredTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "recovered"),
	Help: "Количество восстановленных из зависшего (PIL) состояния задач",
}, []string{"type"})

var startedTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "started"),
	Help: "Количество запущенных задач по типу",
}, []string{"type"})

var startedTotal = prometheus.NewCounter(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "started_total"),
	Help: "Общее количество запущенных задач",
})

var completedTasks = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "completed"),
	Help: "Количество удачно завершенных задач по типу",
}, []string{"type"})

var completedTotal = prometheus.NewCounter(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "completed_total"),
	Help: "Общее Количество удачно завершенных задач",
})

var queuePoolSize = prometheus.NewGauge(prometheus.GaugeOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "pool"),
	Help: "Общий пул задач ожидающий помещения в очередь на исполнение",
})

var queueSize = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: prometheus.BuildFQName("rdlt", "tasks", "queue_size"),
	Help: "Общий пул задач ожидающий помещения в очередь на исполнение",
}, []string{"type"})

var queueAbsDelays = prometheus.NewHistogramVec(prometheus.HistogramOpts{
	Name:    prometheus.BuildFQName("rdlt", "tasks", "absolute_delay"),
	Help:    "Абсолютная задержка выполнения задачи",
	Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10, 60, 600, 1800, 3600},
}, []string{"type"})

var queueSubmitDelays = prometheus.NewHistogramVec(prometheus.HistogramOpts{
	Name:    prometheus.BuildFQName("rdlt", "tasks", "submit_delay"),
	Help:    "Задержка выполнения задачи от момента постановки в очередь исполнения",
	Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10, 60, 600, 1800, 3600},
}, []string{"type"})

var queueTaskRunningTime = prometheus.NewHistogramVec(prometheus.HistogramOpts{
	Name:    prometheus.BuildFQName("rdlt", "tasks", "running_time"),
	Help:    "Время выполнения задачи",
	Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10, 60, 600, 1800, 3600},
}, []string{"type"})

func init() {
	prometheus.MustRegister(retryTasks, completedTotal, droppedTasks, queueLockRetryTasks, completedTasks, recoveredTasks, scheduledTasks, queueSize, queuedTasks, startedTasks, queuePoolSize, queueAbsDelays, queueSubmitDelays, queueTaskRunningTime, startedTotal)
}

func withTaskMeta(ctx context.Context, task *Task) context.Context {
	return context.WithValue(ctx, taskMeta, task)
}

type handlerOptions struct {
	withoutQueues     bool
	concurrency       int
	initialLockTimout *time.Duration
}

type HandlerOptions func(*handlerOptions)

// WithoutQueues отключает блокировку очереди, позволяя задачам из одной очереди выполняться параллельно
var WithoutQueues HandlerOptions = func(options *handlerOptions) {
	options.withoutQueues = true
}

// WithConcurrency задает количество горутин в пуле обработки задач
var WithConcurrency = func(concurrency int) HandlerOptions {
	return func(options *handlerOptions) {
		options.concurrency = concurrency
	}
}

// InitialGrabTimeout устанавливает в контексте обработчика задач лимит времени на захват любых блокировок
func InitialGrabTimeout(timeout time.Duration) HandlerOptions {
	return func(options *handlerOptions) {
		options.initialLockTimout = &timeout
	}
}

// GetTaskMeta возвращает метаданные по задаче (доступны в контексте обработчика внутри HandleTasks)
func GetTaskMeta(ctx context.Context) *Task {
	res := &Task{}
	if tmeta, ok := ctx.Value(taskMeta).(*Task); ok && tmeta != nil {
		res = tmeta
	}
	return res
}

var opts *goredislib.Options

// ShortTypeNameFunction функция для сокращения типа задачи в метке prometheus
var ShortTypeNameFunction func(name string) string

func typeName(name string) string {
	if ShortTypeNameFunction != nil {
		return ShortTypeNameFunction(name)
	}
	return strings.TrimPrefix(name, "gitlab.com/skllzz/back/services/")
}

// PoolSize текущее количество задач ожидающих планировки
func PoolSize(ctx context.Context) (int64, error) {
	if cnt, err := client.ZCard(ctx, allTasksSet).Result(); err != nil {
		return 0, err
	} else {
		return cnt, nil
	}

}

func createTaskPayloadKey(t *pb.Task) string {
	return fmt.Sprintf("%v:%v", taskPayloadPrefix, StrHash512(fmt.Sprintf("%v.%v.%v", t.PayloadType, t.Queue, t.Id)))
}

func CreateTask(queueName, id string, when time.Time, payload interface{}) *Task {
	pt := &pb.Task{
		Id:          id,
		Queue:       queueName,
		When:        timestamppb.New(when),
		PayloadType: PayloadName(payload),
	}
	return &Task{
		Queue:   queueName,
		Id:      id,
		Payload: payload,
		When:    when,
		key:     createTaskPayloadKey(pt),
	}
}

func CreateTaskIfNotExists(queueName, id string, when time.Time, payload interface{}) *Task {
	t := CreateTask(queueName, id, when, payload)
	t.IfNotExists = true
	return t
}

// Cancel отменяет задание если оно еще не в активной стадии исполнения
func (task *Task) Cancel(ctx context.Context) error {
	InitRedis()
	taskPayloadKey := task.key
	if taskPayloadKey == "" {
		return errors.New("task is not created")
	}
	pl := client.TxPipeline()
	pl.ZRem(ctx, allTasksSet, taskPayloadKey)
	pl.Del(ctx, taskPayloadKey)
	if _, err := pl.Exec(ctx); err != nil {
		return err
	}
	return nil
}

type groupOptions struct {
	id string
}

type GroupOption = func(*groupOptions)

// GetGroupId возвращает идентификатор группы задач
func GetGroupId(ctx context.Context) string {
	if groupOpt, ok := ctx.Value(taskGroupId).(groupOptions); ok {
		return groupOpt.id
	}
	return ""
}

// WithGroup задает в контексте группу задач, используется в момент вызова ScheduleWaitTask
func WithGroup(ctx context.Context, opts ...GroupOption) context.Context {
	gopt := groupOptions{id: uuid.NewString()}
	for _, f := range opts {
		f(&gopt)
	}
	return context.WithValue(ctx, taskGroupId, gopt)
}

// WithoutGroup убирает из контекста данные о группу задач если она есть
func WithoutGroup(ctx context.Context) context.Context {
	return context.WithValue(ctx, taskGroupId, nil)
}

var GroupTimeoutError = errors.New("group is timed out")

// WaitGroup ожидает завершения всех задач в группе заданной контекстом
func WaitGroup(ctx context.Context, maxIdleTimeout time.Duration) error {
	InitRedis()
	if groupOpt, ok := ctx.Value(taskGroupId).(groupOptions); ok && groupOpt.id != "" {
		wctx, wcancel := context.WithCancel(ctx)
		defer wcancel()
		wc := getNotifyChannel(wctx, groupOpt.id)
		for ctx.Err() == nil {
			if ev, err := client.Exists(ctx, groupOpt.id).Result(); err != nil {
				return err
			} else {
				if ev == 0 {
					return nil
				}
			}
			idleTimer := time.NewTimer(maxIdleTimeout)
			select {
			case <-idleTimer.C:
				return GroupTimeoutError
			case <-wc:
				break
			case <-ctx.Done():
				break
			}
			idleTimer.Stop()
		}
	}
	return nil
}

func waitKeyGone(ctx context.Context, keyId string) error {
	wctx, wcancel := context.WithCancel(ctx)
	defer wcancel()
	wc := getNotifyChannel(wctx, keyId)
	for ctx.Err() == nil {
		if _, err := client.Get(ctx, keyId).Result(); err != nil {
			if err == goredislib.Nil {
				return nil
			}
			return err
		}
		select {
		case <-wc:
			continue
		case <-ctx.Done():
			return ctx.Err()
		}
	}
	return ctx.Err()

}

// ScheduleTask планирует выполнение задачи в заданное время по стратегии Run At Least Once
// т.е. задача обязательно выполнится не менее одного раза, повторные запуски возможны в случае ошибок в работе с redis
func ScheduleTask(ctx context.Context, task *Task) error {
	InitRedis()
	when := task.When
	if when.IsZero() || when.Before(time.Now()) {
		when = time.Now()
	}
	t := &pb.Task{
		Id:    task.Id,
		Queue: task.Queue,
		When:  timestamppb.New(when),
	}
	if task.Payload != nil {
		if payload, err := Marshal(task.Payload); err != nil {
			return err
		} else {
			t.Payload = payload
			t.PayloadType = PayloadName(task.Payload)
			task.Type = t.PayloadType
		}
	}
	taskPayloadKey := createTaskPayloadKey(t)
	task.key = taskPayloadKey
	if gopt, ok := ctx.Value(taskGroupId).(groupOptions); ok && gopt.id != "" {
		t.Group = gopt.id
		if when.Sub(time.Now()) > 1*time.Minute {
			logging.Default.WithOptions(zap.AddStacktrace(zapcore.WarnLevel), zap.AddCallerSkip(1)).Warn("Group include far future task",
				zap.Any("task", task))
		}
	}
	if payload, err := Marshal(t); err != nil {
		return err
	} else {
		pl := client.TxPipeline()
		ttl := task.When.Sub(time.Now()) + MaxTaskDelayBeforeDrop*2
		if t.Group != "" {
			pl.SAdd(ctx, t.Group, taskPayloadKey)
			pl.PExpire(ctx, t.Group, groupMaxTTL)
		}
		if task.IfNotExists {
			pl.SetNX(ctx, taskPayloadKey, payload, ttl)
			pl.ZAddNX(ctx, allTasksSet, goredislib.Z{
				Score:  float64(when.UnixMilli()),
				Member: taskPayloadKey,
			})
		} else {
			pl.Set(ctx, taskPayloadKey, payload, ttl)
			pl.ZAdd(ctx, allTasksSet, goredislib.Z{
				Score:  float64(when.UnixMilli()),
				Member: taskPayloadKey,
			})
		}
		pl.XAdd(ctx, &goredislib.XAddArgs{
			ID:     "*",
			Stream: tasksChangesStream,
			MinID:  strconv.FormatInt(time.Now().Add(-time.Minute).UnixMilli(), 10),
			Approx: true,
			Values: map[string]interface{}{payloadKeyName: taskPayloadKey},
		})
		if _, err := pl.Exec(ctx); err != nil {
			return err
		}
		scheduledTasks.WithLabelValues(typeName(t.PayloadType)).Inc()
	}
	return nil
}

func runRedisTasks(ctx context.Context) {
	runRedisTasksOnce.Do(func() {
		InitRedis()
		totalTasks := 0
		for ctx.Err() == nil {
			dv := time.Minute
			lastKnownId := "0"
			if err := WithNamedLock(WithoutStats(ctx), taskSupplierLockId, func(ctx context.Context) error {
				if info, err := client.XInfoStream(ctx, tasksChangesStream).Result(); err != nil {
					return err
				} else {
					lastKnownId = info.LastGeneratedID
					if cnt, err := PoolSize(ctx); err != nil {
						return err
					} else {
						queuePoolSize.Set(float64(cnt))
					}
					if tlist, err := client.ZRangeArgsWithScores(ctx, goredislib.ZRangeArgs{
						Key:     allTasksSet,
						ByScore: true,
						Count:   bucketSize,
						Offset:  0,
						Start:   0,
						Stop:    time.Now().Add(dv).UnixMilli(),
					}).Result(); err != nil {
						return err
					} else {
						if len(tlist) > 0 {
							logging.Default.Debug("Enqueue tasks", zap.Int("count", len(tlist)))
						}
						inFuture := false
						for _, z := range tlist {
							pkeyid := fmt.Sprintf("%v", z.Member)
							stime := time.UnixMilli(int64(z.Score))
							delta := stime.Sub(time.Now())
							if delta > 0 {
								if delta < dv {
									dv = delta
								}
								inFuture = true
								continue
							}
							if v, err := client.Get(ctx, pkeyid).Result(); err != nil {
								if err != goredislib.Nil {
									return err
								} else {
									if _, err := client.ZRem(ctx, allTasksSet, pkeyid).Result(); err != nil {
										return err
									}
								}
							} else {
								ptask := &pb.Task{}
								if err := Unmarshal([]byte(v), ptask); err != nil {
									logging.Default.Error("Unable to unmarshal task", zap.Error(err))
								} else {
									id := time.Now().Sub(ptask.When.AsTime())
									pl := client.TxPipeline()
									pl.XAdd(ctx, &goredislib.XAddArgs{
										ID:     "*",
										Values: map[string]interface{}{payloadKeyName: pkeyid},
										Stream: fmt.Sprintf("rdlt_typed_tasks_%v", ptask.PayloadType),
									})
									pl.ZRem(ctx, allTasksSet, pkeyid)
									if _, err := pl.Exec(ctx); err != nil {
										return err
									}
									totalTasks++
									queuedTasks.WithLabelValues(typeName(ptask.PayloadType)).Inc()
									logging.Default.Debug("Moving task",
										zap.Duration("delay", id),
										zap.String("id", ptask.Id),
										zap.String("type", ptask.PayloadType),
									)
								}
							}
						}
						if len(tlist) > 0 && !inFuture {
							dv = 0
						}
					}
				}
				return nil
			}); err != nil {
				if ctx.Err() == nil {
					logging.Default.Warn("Unable to process tasks", zap.Error(err))
				}
				_, _ = client.XAdd(ctx, &goredislib.XAddArgs{
					ID:     "*",
					Stream: tasksChangesStream,
					MaxLen: 10,
					Approx: true,
					Values: map[string]interface{}{"ping": 0},
				}).Result()
				select {
				case <-ctx.Done():
				case <-time.After(5 * time.Second):
				}
			} else {
				if dv > 1*time.Second {
					logging.Default.Debug("Task stats", zap.Int("total_served", totalTasks))
					if dv > time.Minute {
						dv = time.Minute
					}
					logging.Default.Debug("Waiting task changes", zap.Duration("duration", dv))
					if _, err := client.XRead(ctx, &goredislib.XReadArgs{
						Streams: []string{tasksChangesStream, lastKnownId},
						Count:   1,
						Block:   dv,
					}).Result(); err != nil {
						if err != goredislib.Nil && ctx.Err() == nil {
							logging.Default.Warn("Unable to wait tasks", zap.Error(err))
						}
					}
				} else {
					select {
					case <-ctx.Done():
					case <-time.After(dv):
					}
				}
			}
		}
	})
}

// HandleTasks запускает обработчик задач для заданного в параметризации типа данных
// Возвращает функцию которой можно отменит обработчик
func HandleTasks[T any](ctx context.Context, onTask func(ctx context.Context, task *T) error, opts ...HandlerOptions) context.CancelFunc {
	tctx, tcancel := context.WithCancel(ctx)
	go func() {
		payloadName := PayloadName(new(T))
		select {
		case <-redisReady.Done():
		case <-tctx.Done():
		}
		for tctx.Err() == nil {
			if err := handleTasks(tctx, onTask, opts...); err != nil {
				if tctx.Err() == nil {
					logging.Default.Error("Error while handling task", zap.String("type", payloadName), zap.Error(err))
				}
				select {
				case <-tctx.Done():
					return
				case <-time.After(3 * time.Second):
				}
			}
		}
	}()
	return tcancel
}
func handleTasks[T any](ctx context.Context, onTask func(ctx context.Context, task *T) error, opts ...HandlerOptions) error {
	options := &handlerOptions{}
	for _, f := range opts {
		f(options)
	}
	grabTimeout := time.Second * 5
	if options.initialLockTimout != nil {
		grabTimeout = *options.initialLockTimout
	}
	if options.concurrency <= 0 {
		options.concurrency = DefaultConcurrency
	}
	ictx := WithInitialGrabTimeout(ctx, grabTimeout)
	payloadName := PayloadName(new(T))
	shortTypeName := typeName(payloadName)
	me := uuid.NewString()
	streamName := fmt.Sprintf("rdlt_typed_tasks_%v", payloadName)
	groupName := fmt.Sprintf("rdlt_typed_group_%v", payloadName)
	runner := multiop.NewParallelRunner(ctx, options.concurrency)
	defer runner.Dispose()

	if _, err := client.XGroupCreateMkStream(ctx, streamName, groupName, "0-0").Result(); err != nil {
		if !goredislib.HasErrorPrefix(err, "BUSYGROUP") {
			return err
		}
	}
	keepAliveCtx, keepAliveCtxCancel := context.WithCancel(ctx)
	defer keepAliveCtxCancel()
	go func() {
		for keepAliveCtx.Err() == nil {
			if l, err := client.XLen(ctx, streamName).Result(); err == nil {
				queueSize.WithLabelValues(shortTypeName).Set(float64(l))
			}
			select {
			case <-ctx.Done():
				return
			case <-time.After(15 * time.Second):
			}
			_, _ = client.XReadGroup(ctx, &goredislib.XReadGroupArgs{
				Streams:  []string{streamName, "0"},
				Group:    groupName,
				Count:    1,
				Consumer: me,
			}).Result()
		}
	}()
	rescheduleConsumer := func(ctx2 context.Context, consumerName string) (bool, error) {
		if pendings, err := client.XPendingExt(ctx2, &goredislib.XPendingExtArgs{
			Consumer: consumerName,
			Group:    groupName,
			Stream:   streamName,
			Start:    "-",
			End:      "+",
			Count:    bucketSize,
		}).Result(); err != nil {
			return false, err
		} else {
			for _, p := range pendings {
				if p.ID != "" {
					if entries, err := client.XRange(ctx, streamName, p.ID, p.ID).Result(); err != nil {
						return false, err
					} else {
						for _, entry := range entries {
							pl := client.TxPipeline()
							pl.XAck(ctx, streamName, groupName, entry.ID)
							pl.XDel(ctx, streamName, entry.ID)
							pl.XAdd(ctx, &goredislib.XAddArgs{
								ID:     "*",
								Stream: streamName,
								Values: entry.Values,
							})
							if _, err := pl.Exec(ctx); err != nil {
								return false, err
							}
							recoveredTasks.WithLabelValues(shortTypeName).Inc()
						}
					}
				}
			}
			if len(pendings) > 0 {
				return true, nil
			}
		}
		return false, nil
	}
	for ctx.Err() == nil {
		dv := time.Second * 30
		if consumers, err := client.XInfoConsumers(ctx, streamName, groupName).Result(); err != nil {
			return err
		} else {
			for _, consumer := range consumers {
				if consumer.Name == me {
					continue
				}
				if consumer.Idle < time.Minute {
					continue
				}
				fmt.Printf("Orphaned consumer %v pending %v\n", consumer.Name, consumer.Pending)
				if consumer.Pending == 0 {
					if _, err := client.XGroupDelConsumer(ctx, streamName, groupName, consumer.Name).Result(); err != nil {
						return err
					} else {
						logging.Default.Info("Remove orphaned consumer", zap.String("group", groupName), zap.String("consumer", consumer.Name))
					}
				} else {
					if hasMore, err := rescheduleConsumer(ctx, consumer.Name); err != nil {
						return err
					} else {
						dv = time.Second
						if hasMore {
							dv = 0
						}
					}
				}
			}
		}
		if err := client.XTrimMinID(ctx, streamName, strconv.FormatInt(time.Now().Add(-MaxTaskDelayBeforeDrop).UnixMilli(), 10)).Err(); err != nil {
			return err
		}
		if streams, err := client.XReadGroup(ctx, &goredislib.XReadGroupArgs{
			Streams:  []string{streamName, ">"},
			Group:    groupName,
			Count:    bucketSize,
			Block:    dv,
			Consumer: me,
		}).Result(); err != nil {
			if err != goredislib.Nil && ctx.Err() == nil {
				return err
			}
		} else {
			for _, stream := range streams {
				for _, _msg := range stream.Messages {
					msg := _msg
					payloadKey := ""
					if pload_key, ok := msg.Values[payloadKeyName].(string); ok && pload_key != "" {
						payloadKey = pload_key
						if v, err := client.Get(ctx, pload_key).Result(); err != nil {
							if err != goredislib.Nil {
								return err
							}
						} else {
							ptask := &pb.Task{}
							if err := Unmarshal([]byte(v), ptask); err != nil {
								logging.Default.Error("Unable to unmarshal task", zap.Error(err))
							} else {
								if ptask.PayloadType != payloadName {
									logging.Default.Error("Invalid payload type",
										zap.String("current", ptask.PayloadType),
										zap.String("expected", payloadName))
								} else {
									ninst := new(T)
									if err := Unmarshal(ptask.Payload, ninst); err != nil {
										logging.Default.Error("Unable to unmarshal task",
											zap.Any("task", ptask),
											zap.Error(err))
									} else {
										logging.Default.Debug("Ready to run task", zap.Any("task", ptask))
										if err := runner.Submit(func(ctx context.Context) error {

											started := time.Now()
											planned := started
											if v, err := strconv.ParseInt(strings.Split(msg.ID, "-")[0], 10, 64); err == nil {
												planned = time.UnixMilli(v)
											}
											completed := time.Now()
											lockName := ""
											if ptask.Queue != "" && options.withoutQueues == false {
												lockName = fmt.Sprintf("task_queue_%v", ptask.Queue)
											}
											tctx := ictx
											if ptask.Group != "" {
												tctx = context.WithValue(tctx, taskGroupId, groupOptions{
													id: ptask.Group,
												})
												if err := client.PExpire(ctx, ptask.Group, groupMaxTTL).Err(); err != nil {
													return err
												}
											}
											var gone = false
											if err := WithNamedLock(tctx, lockName, func(ctx context.Context) error {
												if c, err := client.Exists(ctx, payloadKey).Result(); err != nil {
													return err
												} else {
													if c == 0 {
														gone = true
														return LockTimeoutError
													}
												}
												if ptask.Group != "" {
													if err := client.PExpire(ctx, ptask.Group, groupMaxTTL).Err(); err != nil {
														return err
													}
												}
												started = time.Now()
												defer func() {
													completed = time.Now()
												}()
												startedTasks.WithLabelValues(shortTypeName).Inc()
												startedTotal.Inc()
												queueSubmitDelays.WithLabelValues(shortTypeName).Observe(started.Sub(planned).Seconds())
												queueAbsDelays.WithLabelValues(shortTypeName).Observe(started.Sub(ptask.When.AsTime()).Seconds())
												return onTask(withTaskMeta(ctx, &Task{
													Id:    ptask.Id,
													Queue: ptask.Queue,
													When:  ptask.When.AsTime(),
													Type:  payloadName,
												}), ninst)
											}); err != nil {
												pl := client.TxPipeline()
												pl.XAck(ctx, streamName, groupName, msg.ID)
												if errors.Is(err, LockTimeoutError) {
													pl.XDel(ctx, streamName, msg.ID)
													// если не удалось забрать блокировку, то сразу поставим в конец очереди
													if !gone {
														queueLockRetryTasks.WithLabelValues(shortTypeName).Inc()
														logging.Default.Debug("Retry task locking",
															zap.String("id", ptask.Id),
															zap.String("type", ptask.PayloadType),
															zap.String("queue", ptask.Queue),
														)
														pl.XAdd(ctx, &goredislib.XAddArgs{
															ID:     "*",
															Stream: streamName,
															Values: msg.Values,
														})
													}
												} else {
													delta := time.Now().Sub(ptask.When.AsTime())
													// задача слишком старая, по этому удаляем ее, но сохраним ее в очереди еще некоторое время на случай разбора полетов
													if delta > MaxTaskDelayBeforeDrop {
														pl.PExpire(ctx, payloadKey, MaxTaskDelayBeforeDrop)
														if ptask.Group != "" {
															pl.SRem(ctx, ptask.Group, payloadKey)
															pl.PExpire(ctx, ptask.Group, groupMaxTTL)
														}
														droppedTasks.WithLabelValues(shortTypeName).Inc()
														logging.Default.WithOptions(logging.NoStackTrace).Error("Drop task error",
															zap.String("id", ptask.Id),
															zap.String("type", ptask.PayloadType),
															zap.String("queue", ptask.Queue),
															zap.Error(err),
														)
													} else {
														retryTasks.WithLabelValues(shortTypeName).Inc()
														// иначе запланируем на попозже
														delta += time.Duration(rand.Int31n(10)) * time.Second
														if delta < time.Duration(10)*time.Second {
															delta = time.Duration(10) * time.Second
														}
														if delta > 5*time.Minute {
															delta = 5*time.Minute + time.Duration(rand.Int31n(30))*time.Second
														}
														newTime := time.Now().Add(delta)
														pl.XDel(ctx, streamName, msg.ID)
														pl.ZAdd(ctx, allTasksSet, goredislib.Z{
															Score:  float64(newTime.UnixMilli()),
															Member: pload_key,
														})
														pl.XAdd(ctx, &goredislib.XAddArgs{
															ID:     "*",
															Stream: tasksChangesStream,
															MaxLen: 10,
															Approx: true,
															Values: map[string]interface{}{payloadKeyName: pload_key},
														})
														logging.Default.WithOptions(logging.NoStackTrace).Error("Retry task error",
															zap.String("id", ptask.Id),
															zap.String("type", ptask.PayloadType),
															zap.String("queue", ptask.Queue),
															zap.Time("on", newTime),
															zap.Error(err),
														)
													}
												}
												if _, err := pl.Exec(ctx); err != nil {
													return err
												}
											} else {
												duration := completed.Sub(started)
												queueTaskRunningTime.WithLabelValues(shortTypeName).Observe(duration.Seconds())
												completedTasks.WithLabelValues(shortTypeName).Inc()
												completedTotal.Inc()
												delay := started.Sub(ptask.When.AsTime())
												// удаляем задачу как полностью обработанную
												pl := client.TxPipeline()
												pl.XAck(ctx, streamName, groupName, msg.ID)
												pl.XDel(ctx, streamName, msg.ID)
												if payloadKey != "" {
													pl.Del(ctx, payloadKey)
												}
												if ptask.Group != "" {
													pl.SRem(ctx, ptask.Group, payloadKey)
													pl.PExpire(ctx, ptask.Group, groupMaxTTL)
												}
												if _, err := pl.Exec(ctx); err != nil {
													return err
												}
												logging.Default.Debug("Complete task",
													zap.String("id", ptask.Id),
													zap.String("type", ptask.PayloadType),
													zap.String("queue", ptask.Queue),
													zap.Duration("delay", delay),
													zap.Duration("took", duration),
												)
											}
											return nil
										}); err != nil {
											return err
										} else {
											// Важно перейти к следующему сообщению оставив это в PIL
											continue
										}
									}
								}
							}
						}
					}
					// Помечаем это сообщение как отработанное если по нему не сформировалась задача
					pl := client.TxPipeline()
					pl.XAck(ctx, streamName, groupName, msg.ID)
					pl.XDel(ctx, streamName, msg.ID)
					if payloadKey != "" {
						pl.Del(ctx, payloadKey)
					}
					if _, err := pl.Exec(ctx); err != nil {
						return err
					}
				}
			}
		}
	}
	/*	go func() {
			for true {
				if hasMore, err := rescheduleConsumer(context.Background(), me); err != nil || !hasMore {
					break
				}
				client.XGroupDelConsumer(ctx, streamName, groupName, me).Result()
			}
		}()
	*/
	return nil
}

// WaitComplete ожидает завершения задания, либо его удаление в результате серии ошибок или отмены
// Для отслеживания завершения группы задач, используйте контекст с WithGroup + WaitGroup
func (task *Task) WaitComplete(ctx context.Context) error {
	InitRedis()
	taskPayloadKey := task.key
	if taskPayloadKey == "" {
		return nil
	}
	return waitKeyGone(ctx, taskPayloadKey)
}
