package rdlt

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/skllzz/jmot/rdlt/logging"
	"gitlab.com/skllzz/multiop"
	"go.uber.org/atomic"
	"go.uber.org/zap"
	"sync"
	"time"
)

var lockCount = prometheus.NewGauge(prometheus.GaugeOpts{
	Name: prometheus.BuildFQName("rdlt", "lock", "count"),
	Help: "Всего блокировок",
})
var lockCountSuccess = prometheus.NewCounter(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "lock", "count_success"),
	Help: "Всего блокировок захвачено",
})
var lockCountError = prometheus.NewCounter(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("rdlt", "lock", "count_error"),
	Help: "Ошибок на блокровках",
})

var lockDelay = prometheus.NewHistogram(prometheus.HistogramOpts{
	Name:    prometheus.BuildFQName("rdlt", "lock", "delay"),
	Help:    "Задержка на блокировке в секундах",
	Buckets: []float64{0, 0.1, 0.5, 1, 2, 4, 10, 20, 30, 60, 120, 300, 600, 1200, 3600},
})

func init() {
	prometheus.MustRegister(lockCount, lockCountError, lockCountSuccess, lockDelay)
}

var contextLocks = "contextLocks"
var withoutStats = "withoutStats"
var initialGrabMutexTimout = "initialGrabMutexTimout"

// AddContextLocks добавляет в контекст список требуемых блокировок имен, игнорируя запросы на ранее полученные выше по контексту
func AddContextLocks(ctx context.Context, names ...string) context.Context {
	lockNames := make(map[string]bool)
	for _, nm := range names {
		if nm != "" {
			lockNames[nm] = true
		}
	}
	for nm, _ := range GetContextLocks(ctx) {
		if nm != "" {
			lockNames[nm] = false
		}
	}
	return context.WithValue(ctx, contextLocks, lockNames)
}

// NamedLocks добавляет в контекст список безусловно требуемых блокировок имен
func NamedLocks(ctx context.Context, names ...string) context.Context {
	lockNames := make(map[string]bool)
	for _, nm := range names {
		if nm != "" {
			lockNames[nm] = true
		}
	}
	return context.WithValue(ctx, contextLocks, lockNames)
}

// GetContextLocks запрашивает из контекста список блокировок
func GetContextLocks(ctx context.Context) map[string]bool {
	if m, ok := ctx.Value(contextLocks).(map[string]bool); ok {
		return m
	}
	return nil
}

// WithNamedLock выполнение блока кода block при условии эксклюзивной блокировки с именем name, если name пустой то блокировка не выполняется
func WithNamedLock(ctx context.Context, name string, block func(context.Context) error) error {
	return WithContextLocks(NamedLocks(ctx, name), block)
}

func WithoutStats(ctx context.Context) context.Context {
	return context.WithValue(ctx, withoutStats, true)
}

// WithInitialGrabTimeout время на первоначальный захват всех блокировок и в случае его превышения возвращает LockTimeoutError
// При duration<=0 то если сразу не получилось заполучить блокировку то возвращается LockTimeoutError
func WithInitialGrabTimeout(ctx context.Context, duration time.Duration) context.Context {
	return context.WithValue(ctx, initialGrabMutexTimout, duration)
}

var LockTimeoutError = errors.New("lock timed out")

// WithContextLocks выполнение блока кода block при условии эксклюзивной блокировки всех не пустых имен в names, если names пустой то блокировка не выполняется
func WithContextLocks(ctx context.Context, block func(context.Context) error) error {
	InitRedis()
	withStats := true
	if nostats, ok := ctx.Value(withoutStats).(bool); ok && nostats {
		withStats = false
	}
	lockNames := GetContextLocks(ctx)
	if lockNames == nil {
		panic(fmt.Errorf("context does not have lock value"))
	}
	lcount := len(lockNames)
	if lcount == 0 {
		return block(ctx)
	}
	lockCount.Add(float64(lcount))
	defer lockCount.Sub(float64(lcount))
	started := time.Now()
	var mutexes = make(map[string]string)
	mlock := new(sync.Mutex)
	lockTimeoutCtx, lockTimoutCancel := context.WithCancel(ctx)
	var getLocks = atomic.NewBool(false)
	if initialGrabTimout, ok := ctx.Value(initialGrabMutexTimout).(time.Duration); ok && initialGrabTimout > 0 {
		go func() {
			select {
			case <-ctx.Done():
				return
			case <-time.After(initialGrabTimout):
			}
			if ctx.Err() == nil && getLocks.Load() == false {
				lockTimoutCancel()
			}
		}()
	}
	defer func() {
		lockTimoutCancel()
		mlock.Lock()
		defer mlock.Unlock()
		for k, v := range mutexes {
			logging.Default.Debug("Unlocking name", zap.String("lock", k))
			if err := releaseLockInContext(context.Background(), k, v); err != nil {
				if BreakableBackgroundContext.Err() == nil {
					logging.Default.Error("unable to unlock mutex", zap.Error(err), zap.String("mutex", k))
				}
			} else {
				logging.Default.Debug("Unlock name", zap.String("lock", k))
			}
		}
	}()
	logging.Default.Debug("Lock names", zap.Any("locks", lockNames))
	lockRunner := multiop.NewParallelRunner(ctx, 16)
	defer lockRunner.Dispose()
	for nm_, newLock := range lockNames {
		lockName := nm_
		keyLockName := fmt.Sprintf("gitlab.com/skllzz/jmot/rdlt:%v", nm_)
		if newLock {
			if err := lockRunner.Submit(func(ctx context.Context) error {
				lockUid := uuid.NewString()
				if err := grabLockInContext(lockTimeoutCtx, keyLockName, lockUid, func() {
					lockTimoutCancel()
				}); err != nil {
					if lockTimeoutCtx.Err() == nil {
						lockCountError.Inc()
					}
					return err
				} else {
					logging.Default.Debug("Locked name", zap.String("lock", lockName))
					mlock.Lock()
					mutexes[keyLockName] = lockUid
					mlock.Unlock()
					lockCountSuccess.Inc()
				}
				return nil
			}); err != nil {
				return err
			}
		}
	}
	if err := lockRunner.Await(); err != nil {
		if lockTimeoutCtx.Err() != nil {
			err = LockTimeoutError
		}
		return err
	}
	getLocks.Store(true)
	if withStats {
		dv := time.Now().Sub(started)
		if dv > 3*time.Second {
			logging.Default.Warn(
				"Lock delay too high",
				zap.Duration("duration", dv),
				zap.Any("names", lockNames))
		}
		lockDelay.Observe(dv.Seconds())
	}
	return block(lockTimeoutCtx)

}
